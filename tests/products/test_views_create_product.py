from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model

from backend.products.models import Product

UserModel = get_user_model()


class ProductListCreateApiViewTests(APITestCase):
    def setUp(self):
        self.user = UserModel.objects.create_user(
            username='testuser',
            email='testuser@example.com',
            password='testpassword'
        )

        self.client.force_authenticate(user=self.user)

    def test_create_product(self):
        product_data = {
            'code': 'P001',
            'name': 'Test Product',
            'description': 'This is a test product.',
            'price': 19.99,
            'image01': 'image_url.jpg',
            'image01ContentType': 'image/jpeg',
            'owner': self.user.id,
        }

        response = self.client.post(reverse('product_add_api'), product_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(Product.objects.count(), 1)

        self.assertEqual(response.data['code'], product_data['code'])
        self.assertEqual(response.data['name'], product_data['name'])

    def test_invalid_product_data(self):
        invalid_product_data = {
            'code': 'P001',
            'name': '',
            'description': 'This is a test product.',
            'price': 19.99,
            'image01': 'image_url.jpg',
            'image01ContentType': 'image/jpeg',
            'owner': self.user.id,
        }

        response = self.client.post(reverse('product_add_api'), invalid_product_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(Product.objects.count(), 0)

        self.assertIn('name', response.data)

    def test_get_product_list(self):
        Product.objects.create(
            code='P001',
            name='Product 1',
            description='Description 1',
            price=19.99,
            image_01='image_url_1.jpg',
            image_01_content_type='image/jpeg',
            owner=self.user
        )
        Product.objects.create(
            code='P002',
            name='Product 2',
            description='Description 2',
            price=29.99,
            image_01='image_url_2.jpg',
            image_01_content_type='image/jpeg',
            owner=self.user
        )

        response = self.client.get(reverse('products_count_view'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2)

