from django.urls import reverse
from rest_framework import status
from rest_framework.reverse import reverse_lazy
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model

from backend.products.models import Product

UserModel = get_user_model()


class ProductListCreateApiViewTests(APITestCase):
    def setUp(self):
        self.user = UserModel.objects.create_user(
            username='testuser',
            email='testuser@example.com',
            password='testpassword'
        )

        product_data = {
            'code': 'P001',
            'name': 'Test Product',
            'description': 'This is a test product.',
            'price': 19.99,
            'image_01': 'image_url.jpg',
            'image_01_content_type': 'image/jpeg',
            'owner': self.user,
        }
        self.product = Product.objects.create(**product_data)
        self.details_update_delete_url = reverse('product_details_update_delete_api', kwargs={'pk': self.product.pk})

    def test_update_not_authenticated(self):
        new_price = {'price': 20.99}

        response = self.client.patch(self.details_update_delete_url, new_price, format='json')

        self.assertEqual(response.status_code, 401)

    def test_update_product_price(self):
        self.client.force_authenticate(user=self.user)
        new_price = {'price': 20.99}

        response = self.client.patch(self.details_update_delete_url, new_price, format='json')

        self.product.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(new_price['price'], self.product.price)



