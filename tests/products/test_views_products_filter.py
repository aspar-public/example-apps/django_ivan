from django.urls import reverse
from rest_framework import status
from rest_framework.reverse import reverse_lazy
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model

from backend.products.models import Product

UserModel = get_user_model()


class ProductListCreateApiViewTests(APITestCase):
    def setUp(self):
        self.user = UserModel.objects.create_user(
            username='testuser',
            email='testuser@example.com',
            password='testpassword',
        )

        self.products = {1: None, 2: None}
        for i in range(1, 3):
            self.products[i] = Product.objects.create(**{
                'code': f'P00{i}',
                'name': f'Test Product - {i}',
                'description': 'This is a test product.',
                'price': 19.99,
                'image_01': '',
                'image_01_content_type': 'image/jpeg',
                'owner': self.user,
            })
        # self.details_update_delete_url = reverse('product_details_update_delete_api', kwargs={'pk': self.product.pk})
        # self.client.force_authenticate(user=self.user)

    def get_filter_url(self, query_params):
        return reverse("product_add_api") + f"?{query_params}"

    def test_user_not_authenticated(self):
        url = self.get_filter_url(
            f'code.contains={self.products[1].code}'
        )
        response = self.client.get(url)

        self.assertEqual(response.status_code, 401)

    def test_product_filter_contains(self):
        url = self.get_filter_url(
            f'code.contains={self.products[1].code}'
        )
        self.client.force_authenticate(user=self.user)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["code"], self.products[1].code)

    def test_product_filter_does_not_contain(self):
        url = self.get_filter_url(
            f"code.doesNotContain={self.products[1].code[:-1]}")

        self.client.force_authenticate(user=self.user)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 0)

    def test_product_filter_equals(self):
        url = self.get_filter_url(
            f"code.equals={self.products[1].code}")

        self.client.force_authenticate(user=self.user)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["code"], self.products[1].code)

    def test_product_filter_not_equals(self):
        url = self.get_filter_url(
            f"code.notEquals={self.products[1].code}")

        self.client.force_authenticate(user=self.user)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["code"], self.products[2].code)

    def test_product_filter_specified(self):
        url = self.get_filter_url(
            f"owner.specified=False")

        self.client.force_authenticate(user=self.user)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 2)

    def test_product_filter_in(self):
        url = self.get_filter_url(
            f"code.in={self.products[1].code},invalid,invalid1")

        self.client.force_authenticate(user=self.user)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 1)

    def test_produc_filter_not_in(self):
        url = self.get_filter_url(
            f"code.notIn=invalid,invalid1")

        self.client.force_authenticate(user=self.user)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 2)

    def test_product_filter_empty_parameters(self):
        url = self.get_filter_url("")

        self.client.force_authenticate(user=self.user)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 2)

    def test_invalid_field(self):
        self.client.force_authenticate(user=self.user)

        invalid_field = 'cde'
        url = self.get_filter_url(f'{invalid_field}.contains={self.products[1].code}')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 400)

        expected_error_message = f'The field value "{invalid_field}" is not supported.'
        self.assertEqual(response.data['error'], expected_error_message)

    def test_invalid_operator(self):
        self.client.force_authenticate(user=self.user)

        invalid_operator = 'cntains'
        url = self.get_filter_url(f'code.{invalid_operator}={self.products[1].code}')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 400)

        expected_error_message = f'The lookup operator "{invalid_operator}" is not supported.'
        self.assertEqual(response.data['error'], expected_error_message)
