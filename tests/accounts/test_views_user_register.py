from django.core import mail
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase
from django.contrib.auth import get_user_model

from backend.accounts.serializers import UserRegisterSerializer
from tests.user_data import user_data

UserModel = get_user_model()


class UserRegistrationTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.register_url = reverse('user_register_api')

    def test_user_registration_valid_data(self):
        response = self.client.post(self.register_url, user_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('id', response.data)
        self.assertIn('activated', response.data)
        self.assertIn('createdBy', response.data)

        user = UserModel.objects.get(username=user_data['login'])
        self.assertEqual(user.email, user_data['email'])
        self.assertTrue(user.check_password(user_data['password']))
        self.assertEqual(len(mail.outbox), 1)

    def test_user_registration_missing_data(self):
        invalid_data = {
            'login': 'invalidData'
        }

        response = self.client.post(self.register_url, invalid_data, format='json')
        self.assertEqual(len(mail.outbox), 0)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_registration_invalid_first_name(self):
        new_user_data = user_data.copy()
        new_user_data['firstName'] = 'invalid' * 10

        response = self.client.post(self.register_url, new_user_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        expected_error_message = {"firstName": ["Ensure this field has no more than 50 characters."]}
        self.assertEqual(response.json(), expected_error_message)
        self.assertEqual(len(mail.outbox), 0)

    def test_user_registration_invalid_last_name(self):
        new_user_data = user_data.copy()
        new_user_data['lastName'] = 'invalid' * 10

        response = self.client.post(self.register_url, new_user_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        expected_error_message = {"lastName": ["Ensure this field has no more than 50 characters."]}
        self.assertEqual(response.json(), expected_error_message)

    def test_user_registration_invalid_login(self):
        new_user_data = user_data.copy()
        new_user_data['login'] = 'invalid' * 10

        response = self.client.post(self.register_url, new_user_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        expected_error_message = {"login": ["Ensure this field has no more than 50 characters."]}
        self.assertEqual(response.json(), expected_error_message)

    def test_user_registration_invalid_email(self):
        new_user_data = user_data.copy()
        new_user_data['email'] = 'invalid'

        response = self.client.post(self.register_url, new_user_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        expected_error_message = {"email": ["Enter a valid email address."]}
        self.assertEqual(response.json(), expected_error_message)

    def test_user_registration_invalid_password(self):
        new_user_data = user_data.copy()
        new_user_data['password'] = 1

        response = self.client.post(self.register_url, new_user_data, format='json')

        first_error_message = "This password is too short. It must contain at least 8 characters."
        second_error_message = "This password is too common."
        third_error_message = "This password is entirely numeric."
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["password"][0], first_error_message)
        self.assertEqual(response.data["password"][1], second_error_message)
        self.assertEqual(response.data["password"][2], third_error_message)

    def test_user_registration_invalid_password_too_long(self):
        new_user_data = user_data.copy()
        new_user_data['password'] = 'a' * 68

        response = self.client.post(self.register_url, new_user_data, format='json')

        expected_error_message = {"password": [
            f"Password must be less than 67 characters long.(Your is {len(new_user_data['password'])} characters)"
        ]}
        self.assertEqual(response.json(), expected_error_message)

    def test_user_registration_with_same_email(self):
        login = user_data['login']
        email = user_data['email']
        UserModel.objects.create_user(
            username=login,
            email=email,
            password=user_data['password']
        )

        new_user_data = user_data.copy()
        new_user_data['login'] = login + 'X'

        response = self.client.post(self.register_url, new_user_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('email', response.data)
        self.assertEqual(response.data['email'][0], 'Email address is already in use.')

    def test_user_registration_with_same_login(self):
        login = user_data['login']
        email = user_data['email']
        UserModel.objects.create_user(
            username=login,
            email=email,
            password=user_data['password']
        )

        new_user_data = user_data.copy()
        new_user_data['email'] = 'X' + email

        response = self.client.post(self.register_url, new_user_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('login', response.data)
        self.assertEqual(response.data['login'][0], 'Login is already in use.')

    def test_valid_user_data_serializer(self):
        serializer = UserRegisterSerializer(data=user_data)

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.errors, {})

    def test_invalid_user_data_serializer(self):
        user_data_invalid = user_data.copy()
        user_data_invalid['email'] = 'invalid-email'
        user_data_invalid['langKey'] = 'invalid-language'
        user_data_invalid['password'] = 'weak'
        serializer = UserRegisterSerializer(data=user_data_invalid)

        self.assertFalse(serializer.is_valid())
        self.assertEqual(serializer.errors['email'][0], 'Enter a valid email address.')
        self.assertEqual(serializer.errors['langKey'][0], '"invalid-language" is not a valid choice.')
        self.assertEqual(
            serializer.errors['password'][0],
            'This password is too short. It must contain at least 8 characters.'
        )

    def test_missing_required_field(self):
        incomplete_data = user_data.copy()
        incomplete_data.pop('password', None)

        serializer = UserRegisterSerializer(data=incomplete_data)

        self.assertFalse(serializer.is_valid())
        self.assertIn('password', serializer.errors)
        self.assertEqual(serializer.errors['password'][0], 'This field is required.')


