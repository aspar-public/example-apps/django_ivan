from django.urls import reverse
from rest_framework.test import APIClient, APITestCase
from django.contrib.auth import get_user_model

from tests.user_data import user_data

UserModel = get_user_model()


class UserDetailsUpdateDeleteTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.details_update_delete_url = reverse('user_details_update_delete_api')

        self.active_user = user_data.copy()
        self.user = UserModel.objects.create_user(username=self.active_user['login'],
                                                  email=self.active_user['email'],
                                                  password=self.active_user['password'],
                                                  is_active=True,
                                                  )

        self.client.force_authenticate(user=self.user)

    def test_update_user_first_name(self):
        new_first_name = {'firstName': 'Ivannn'}
        response = self.client.patch(self.details_update_delete_url, new_first_name, format='json')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(new_first_name['firstName'], self.user.first_name)

