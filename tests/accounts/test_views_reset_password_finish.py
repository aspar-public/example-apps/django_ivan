from django.urls import reverse
from django.utils import timezone
from rest_framework.test import APIClient, APITestCase
from django.contrib.auth import get_user_model
from tests.user_data import user_data
from backend.core.helpers import generate_key

UserModel = get_user_model()


class UserChangePasswordTests(APITestCase):
    def setUp(self):
        self.client = APIClient()

        self.active_user = user_data.copy()
        self.user = UserModel.objects.create_user(username=self.active_user['login'],
                                                  email=self.active_user['email'],
                                                  password=self.active_user['password'],
                                                  is_active=True,
                                                  reset_key=generate_key(),
                                                  reset_date=timezone.now()
                                                  )
        self.reset_password_url = reverse('password_reset_confirm', kwargs={"key": self.user.reset_key})
        self.client.force_authenticate(user=self.user)

    def test_password_reset_finish_successfully(self):
        data = {
            "newPassword": "asdqwe!@3",
            'key': self.user.reset_key
        }

        response = self.client.post(self.reset_password_url, data, format="json")
        self.assertEqual(response.status_code, 200)

        self.user.refresh_from_db()
        self.assertTrue(self.user.check_password("asdqwe!@3"))
        self.assertIsNone(self.user.reset_key)

    def test_password_reset_finish_wrong_key(self):
        data = {
            "newPassword": "asdqwe!@3",
            'key': 'wrong_key'
        }

        response = self.client.post(self.reset_password_url, data, format="json")
        self.assertEqual(response.status_code, 400)
        expected_message = "This key is not valid. Please enter the valid key or make new request before proceeding!"
        self.assertTrue(response.data['non_field_errors'][0], expected_message)
        self.assertIsNotNone(self.user.reset_key)

    def test_password_reset_finish_expired_key(self):
        expired_date = self.user.reset_date - timezone.timedelta(hours=25)
        self.user.reset_date = expired_date
        self.user.save()
        data = {
            "newPassword": "asdqwe!@3",
            'key': self.user.reset_key
        }

        response = self.client.post(self.reset_password_url, data, format="json")
        self.assertEqual(response.status_code, 400)
        expected_message = 'Reset key has expired.'
        self.assertTrue(response.data['non_field_errors'][0], expected_message)
