from django.core import mail
from django.urls import reverse
from rest_framework.test import APIClient, APITestCase
from django.contrib.auth import get_user_model

from tests.user_data import user_data

UserModel = get_user_model()


class UserChangePasswordTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.reset_password_url = reverse('password_reset_request')

        self.active_user = user_data.copy()
        self.user = UserModel.objects.create_user(username=self.active_user['login'],
                                                  email=self.active_user['email'],
                                                  password=self.active_user['password'],
                                                  # is_active=True,
                                                  )
        self.client.force_authenticate(user=self.user)

    def test_password_reset_init_successfully(self):
        data = {
            "email": self.user.email
        }
        response = self.client.post(self.reset_password_url, data, format="json")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 1)

    def test_password_reset_init_non_existing_email(self):
        data = {
            "email": 'non_existing@example.com'
        }

        response = self.client.post(self.reset_password_url, data, format="json")

        expected_message = "User with this email does not exist."

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data['non_field_errors'][0], expected_message)

    def test_password_reset_init_wrong_email_format(self):
        data = {
            "email": 'non_existing.example.com'
        }

        response = self.client.post(self.reset_password_url, data, format="json")

        expected_message = "Enter a valid email address."

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data['email'][0], expected_message)
