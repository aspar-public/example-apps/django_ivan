from django.urls import reverse
from rest_framework.test import APIClient, APITestCase
from django.contrib.auth import get_user_model

from tests.user_data import user_data

UserModel = get_user_model()


class UserChangePasswordTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.change_password_url = reverse('change-password')

        self.active_user = user_data.copy()
        self.user = UserModel.objects.create_user(username=self.active_user['login'],
                                                  email=self.active_user['email'],
                                                  password=self.active_user['password'],
                                                  is_active=True,
                                                  )

        self.client.force_authenticate(user=self.user)

    def test_user_change_password_successfully(self):
        data = {
            'old_password': self.active_user['password'],
            'new_password': 'new_password',
            'confirm_password': 'new_password'
        }
        response = self.client.patch(
            self.change_password_url,
            data,
            format='json'
        )

        expected_message = "Password changed successfully."
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['detail'], expected_message)
        self.user.refresh_from_db()
        self.assertTrue(self.user.check_password('new_password'))

    def test_user_change_password_wrong_old_password(self):
        data = {
            'old_password': 'old_password',
            'new_password': 'new_password',
            'confirm_password': 'new_password'
        }
        response = self.client.patch(
            self.change_password_url,
            data,
            format='json'
        )

        expected_message = "Incorrect old password."
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data['non_field_errors'][0], expected_message)

    def test_user_change_password_new_password_and_confirm_password_dont_match(self):
        data = {
            'old_password': self.active_user['password'],
            'new_password': 'new_assword',
            'confirm_password': 'new_password'
        }
        response = self.client.patch(
            self.change_password_url,
            data,
            format='json'
        )

        expected_message = "New password and confirm password do not match."
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data['non_field_errors'][0], expected_message)
