from django.urls import reverse
from rest_framework.test import APIClient, APITestCase
from django.contrib.auth import get_user_model

from tests.user_data import user_data

UserModel = get_user_model()


class UserFilterTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.filter_url = reverse('admin_user_list')
        self.login_data = {'username': user_data['login'],
                           'password': user_data['password'],
                           "rememberMe": True,
                           }
        active_user = user_data.copy()
        self.admin_user = UserModel.objects.create_superuser(username='admin',
                                                             email='admin@example.com',
                                                             password='admin!@#',
                                                             is_active=True
                                                             )
        self.users = {1: None, 2: None}
        for i in range(1, 3):
            self.users[i] = UserModel.objects.create_user(username=f"{i}{active_user['login']}",
                                                          email=f"{i}{active_user['email']}",
                                                          password=active_user['password'],
                                                          first_name=f"{i}{active_user['firstName']}",
                                                          last_name=f"{i}{active_user['lastName']}",
                                                          is_active=True
                                                          )

    def get_filter_url(self, query_params):
        return reverse("admin_user_list") + f"?{query_params}"

    def test_user_not_authenticated(self):
        url = self.get_filter_url(
            f"login.contains={self.users[1].username}")
        response = self.client.get(url)

        self.assertEqual(response.status_code, 401)

    def test_user_no_admin_permission(self):
        self.client.force_authenticate(user=self.users[1])
        url = self.get_filter_url(
            f"login.contains={self.users[1].username}")
        response = self.client.get(url)
        expected_message = "You do not have permission to perform this action."
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.data['detail'], expected_message)

    def test_user_filter_contains(self):
        url = self.get_filter_url(
            f"login.contains={self.users[1].username}&firstName.contains={self.users[1].first_name}&lastName.contains={self.users[1].last_name}")

        self.client.force_authenticate(user=self.admin_user)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["login"], self.users[1].username)

    def test_user_filter_does_not_contain(self):
        url = self.get_filter_url(
            f"login.doesNotContain={self.users[1].username[1:]}")

        self.client.force_authenticate(user=self.admin_user)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["login"], 'admin')

    def test_user_filter_equals(self):
        url = self.get_filter_url(
            f"login.equals={self.users[1].username}")

        self.client.force_authenticate(user=self.admin_user)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["login"], self.users[1].username)

    def test_user_filter_not_equals(self):
        url = self.get_filter_url(
            f"login.notEquals={self.users[1].username}&login.notEquals={self.users[2].username}")

        self.client.force_authenticate(user=self.admin_user)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["login"], self.admin_user.username)

    def test_user_filter_specified(self):
        url = self.get_filter_url(
            f"firstName.specified=False")

        self.client.force_authenticate(user=self.admin_user)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 2)

    def test_user_filter_in(self):
        url = self.get_filter_url(
            f"firstName.in={self.users[1].first_name},invalid,invalid1")

        self.client.force_authenticate(user=self.admin_user)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 1)

    def test_user_filter_not_in(self):
        url = self.get_filter_url(
            f"firstName.notIn={self.users[1].first_name},invalid,invalid1")

        self.client.force_authenticate(user=self.admin_user)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 2)

    def test_user_filter_empty_parameters(self):
        url = self.get_filter_url("")

        self.client.force_authenticate(user=self.admin_user)

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 3)

    def test_invalid_field(self):
        self.client.force_authenticate(user=self.admin_user)

        invalid_field = 'lgin'
        url = self.get_filter_url(f'{invalid_field}.contains={self.users[1].username}')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 400)

        expected_error_message = f'The field value "{invalid_field}" is not supported.'
        self.assertEqual(response.data['error'], expected_error_message)

    def test_invalid_operator(self):
        self.client.force_authenticate(user=self.admin_user)

        invalid_operator = 'cntains'
        url = self.get_filter_url(f'login.{invalid_operator}={self.users[1].username}')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 400)

        expected_error_message = f'The lookup operator "{invalid_operator}" is not supported.'
        self.assertEqual(response.data['error'], expected_error_message)

    def test_invalid_format(self):
        self.client.force_authenticate(user=self.admin_user)

        invalid_format = 'logincontains'
        url = self.get_filter_url(f'{invalid_format}={self.users[1].username}')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 400)

        expected_error_message = f'Not supported format in \"{invalid_format}=\"! It should be ?' \
                                 f'[some-field.search-operator]=[some-value]&' \
                                 f'[some-field.search-operator]=[some-value]...'
        self.assertEqual(response.data['error'], expected_error_message)

    def test_invalid_option_for_specified(self):
        self.client.force_authenticate(user=self.admin_user)

        invalid_option = 'Falsee'
        url = self.get_filter_url(f'firstName.specified={invalid_option}')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 400)

        expected_error_message = f"The option \"{invalid_option}\" for lookup operator \"specified\" is not supported. It must be \"True\" or \"False\"."
        self.assertEqual(response.data['error'], expected_error_message)
