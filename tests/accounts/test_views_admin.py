from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from django.test import Client

UserModel = get_user_model()


class CustomUserAdminAPITest(APITestCase):
    def setUp(self):
        self.admin_user = UserModel.objects.create_superuser(
            username='admin',
            email='admin@example.com',
            password='adminpassword'
        )

        self.client = Client()
        self.client.login(username='admin', password='adminpassword')


    def test_custom_user_admin_add_user(self):
        response = self.client.get('/admin/accounts/user/add/')

        self.assertEqual(response.status_code, 200)

        new_user_data = {
            'username': 'testuser',
            'email': 'testuser@example.com',
            'password1': 'testpassword',
            'password2': 'testpassword',
            'is_staff': True,
            'is_active': True,
        }
        response = self.client.post('/admin/accounts/user/add/', new_user_data)

        new_user = UserModel.objects.get(username='testuser')
        self.assertIsNotNone(new_user)

    def test_custom_user_admin_edit_user(self):
        user_to_edit = UserModel.objects.create_user(
            username='editableuser',
            email='editableuser@example.com',
            password='editablepassword'
        )

        response = self.client.get(f'/admin/accounts/user/{user_to_edit.id}/change/')

        self.assertEqual(response.status_code, 200)

        edited_user_data = {
            'username': 'editeduser',
            'email': 'editeduser@example.com',
            'is_staff': True,
            'is_active': False,
        }
        response = self.client.post(f'/admin/accounts/user/{user_to_edit.id}/change/', edited_user_data)

        edited_user = UserModel.objects.get(id=user_to_edit.id)
        self.assertEqual(edited_user.username, 'editeduser')
        self.assertEqual(edited_user.email, 'editeduser@example.com')
        self.assertTrue(edited_user.is_staff)
        self.assertFalse(edited_user.is_active)
