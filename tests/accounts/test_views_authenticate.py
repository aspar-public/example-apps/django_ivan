from datetime import datetime, timedelta

from django.urls import reverse
from rest_framework.test import APIClient, APITestCase
from django.contrib.auth import get_user_model

from tests.user_data import user_data

UserModel = get_user_model()


class UserRegistrationTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.login_url = reverse('user_login_api')
        self.login_data = {'username': user_data['login'],
                           'password': user_data['password'],
                           "rememberMe": True,
                           }
        active_user = user_data.copy()
        # active_user['is_active'] = True
        self.user = UserModel.objects.create_user(username=active_user['login'],
                                                  email=active_user['email'],
                                                  password=active_user['password'],
                                                  is_active=True
                                                  )
        # self.user.is_active = True

    def test_user_cannot_authenticate_not_active_user(self):
        self.user.is_active = False
        self.user.save()

        response = self.client.post(
            self.login_url,
            self.login_data,
            format='json'
        )

        self.assertEqual(response.status_code, 401)

    def test_user_can_authenticate_active_user(self):
        response = self.client.post(
            self.login_url,
            self.login_data,
            format='json'
        )
        self.assertEqual(response.status_code, 200)

    def test_user_authenticate_remember_me(self):
        response = self.client.post(
            self.login_url,
            self.login_data,
            format='json'
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("id_token", response.data)
        self.assertIn('expiresAt', response.data)
        expires_at = response.data["expiresAt"]
        expiration_threshold = datetime.now() + timedelta(days=1)
        self.assertLessEqual(expires_at, expiration_threshold)

    def test_user_authenticate_not_remember_me(self):
        new_login_data = self.login_data.copy()
        new_login_data['rememberMe'] = False
        response = self.client.post(
            self.login_url,
            new_login_data,
            format='json'
        )

        self.assertEqual(response.status_code, 200)
        self.assertIn("id_token", response.data)
        self.assertIn('expiresAt', response.data)
        expires_at = response.data["expiresAt"]
        expiration_threshold = datetime.now() + timedelta(hours=1)
        self.assertLessEqual(expires_at, expiration_threshold)

    def test_user_authenticate_with_invalid_password(self):
        new_login_data = self.login_data.copy()
        new_login_data['password'] = 'invalid'
        response = self.client.post(self.login_url, new_login_data, format="json")

        self.assertEqual(response.status_code, 401)
        self.assertNotIn("id_token", response.data)

    def test_user_authenticate_with_invalid_username(self):
        new_login_data = self.login_data.copy()
        new_login_data['username'] = 'invalid'
        response = self.client.post(self.login_url, new_login_data, format="json")

        self.assertEqual(response.status_code, 401)
        self.assertNotIn("id_token", response.data)

