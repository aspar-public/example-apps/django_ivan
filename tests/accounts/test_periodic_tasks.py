from django.contrib.auth import get_user_model
from django.utils import timezone
from freezegun import freeze_time

from backend.accounts.tasks import delete_unconfirmed_users
from rest_framework.test import APIClient, APITestCase

from tests.user_data import user_data


UserModel = get_user_model()


class DeleteUnconfirmedUsersTaskTest(APITestCase):
    def setUp(self):
        self.client = APIClient()

        active_user = user_data.copy()
        self.user = UserModel.objects.create_user(username=active_user['login'],
                                                  email=active_user['email'],
                                                  password=active_user['password'],
                                                  created_date=timezone.now() - timezone.timedelta(hours=2)
                                                  )

    @freeze_time(timezone.timedelta(hours=12))
    def test_delete_unconfirmed_users(self):
        self.user.created_date = timezone.now() - timezone.timedelta(hours=2)

        delete_unconfirmed_users()

        user_exists = UserModel.objects.filter(username=self.user.username).exists()
        self.assertFalse(user_exists)
