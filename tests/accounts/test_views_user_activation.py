from django.urls import reverse
from rest_framework.test import APIClient, APITestCase
from django.contrib.auth import get_user_model

from tests.user_data import user_data

UserModel = get_user_model()


class UserActivationTests(APITestCase):
    def setUp(self):
        self.client = APIClient()

        active_user = user_data.copy()
        self.user = UserModel.objects.create_user(username=active_user['login'],
                                                  email=active_user['email'],
                                                  password=active_user['password'],
                                                  )

        key = self.user.activation_key
        self.activation_url = reverse('user_verify_email', kwargs={"key": key})
        self.login_data = {'username': user_data['login'],
                           'password': user_data['password'],
                           "rememberMe": True,
                           }

    def test_user_activation_success(self):
        self.assertFalse(self.user.is_active)

        response = self.client.get(
            self.activation_url,
        )
        success_message = 'Email successfully verified.'

        self.assertEqual(response.status_code, 200)
        self.assertIn('detail', response.data)
        self.assertEqual(response.data['detail'], success_message)

        self.user.refresh_from_db()
        self.assertTrue(self.user.is_active)

    def test_user_activation_fail(self):
        url = reverse('user_verify_email', kwargs={"key": 'invalid'})
        response = self.client.get(
            url,
        )

        fail_message = 'Not found a valid key.'

        self.assertEqual(response.status_code, 400)
        self.assertIn('error', response.data)
        self.assertEqual(response.data['error'], fail_message)
        self.user.refresh_from_db()
        self.assertFalse(self.user.is_active)



