FROM python:3.10.2-slim

WORKDIR /django_ivan

COPY . /django_ivan

RUN python -m venv venv

SHELL ["/bin/bash", "-c"]
RUN if [ -f "venv/bin/activate" ]; then source venv/bin/activate; else source venv/Scripts/activate; fi

RUN pip install -r requirements.txt

EXPOSE 8000

RUN python manage.py migrate

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
