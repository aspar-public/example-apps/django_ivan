from django.contrib.auth import base_user
from django.contrib.auth.models import Group


class AppUserManager(base_user.BaseUserManager):

    def create_user(self, username, email, password, **extra_fields):
        if not username:
            raise ValueError('The Username field must be set')
        if not email:
            raise ValueError('The Email field must be set')
        if not password:
            raise ValueError('The Password field must be set')

        email = self.normalize_email(email)
        user = self.model(username=username, email=email, **extra_fields)

        user.set_password(password)
        user.save()

        role_user_group, created = Group.objects.get_or_create(name='ROLE_USER')
        user.groups.add(role_user_group)

        return user

    def create_superuser(self, username, email, password=None, **extra_fields):
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_active", True)

        if not username:
            raise ValueError('The Login field must be set')

        if not email:
            raise ValueError('The Email field must be set')

        if not password:
            raise ValueError('The Password field must be set')

        user = self.create_user(username, email, password, **extra_fields)
        admin_user_group, created = Group.objects.get_or_create(name='ROLE_ADMIN')
        user.groups.add(admin_user_group)

        return user
