from rest_framework.exceptions import ValidationError


def validate_password_max_len(value):
    MAX_PASSWORD_LEN = 67

    if len(value) > MAX_PASSWORD_LEN:
        raise ValidationError(
            f"Password must be less than {MAX_PASSWORD_LEN} characters long.(Your is {len(value)} characters)")

    return value


def validate_filtering_data(initial_data, lookups_mapping, field_mapping):
    param = initial_data['param']

    if param in ['page', 'page_size']:
        return initial_data

    parts = param.split('.')
    values = initial_data['values']

    if not len(parts) == 2:
        raise ValueError(
            f'Not supported format in "{param}="! It should be '
            f'?[some-field.search-operator]=[some-value]'
            f'&[some-field.search-operator]=[some-value]...'
        )

    field_name, lookup = parts

    if lookup == 'specified' and values[0].lower() not in ['true', 'false']:
        raise ValueError(
            f'The option "{values[0]}" for lookup operator "specified" is not supported. It must be "True" or "False".')
    if field_name not in field_mapping:
        raise ValueError(f'The field value "{field_name}" is not supported.')

    if lookup not in lookups_mapping:
        raise ValueError(f'The lookup operator "{lookup}" is not supported.')

    field_name = field_mapping.get(field_name, field_name)
    mapped_lookup = lookups_mapping.get(lookup, lookup)
    lookup_expr = f'{field_name}__{mapped_lookup}'

    validated_data = {
        'lookup_expression': lookup_expr,
        'lookup': lookup,
        'values': values
    }

    return validated_data
