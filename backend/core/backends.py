from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

UserModel = get_user_model()


class EmailAuthBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        if not username or not password:
            return None

            # First, try to authenticate using the provided username
        if username:
            try:
                user = UserModel._default_manager.get_by_natural_key(username)
            except UserModel.DoesNotExist:
                user = None
        else:
            user = None

            # If username authentication fails or no username was provided, try email
        if not user:
            try:
                user = UserModel.objects.get(email=username)
            except UserModel.DoesNotExist:
                user = None

        if user and user.check_password(password):
            return user
        return None

    def authenticate_header(self, request):
        return 'Bearer realm="api"'
