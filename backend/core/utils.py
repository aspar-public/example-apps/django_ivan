from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from backend import settings


def send_email(data):
    user = data['user']
    email_content = render_to_string(data['html_template'], {
        'user': user,
        'server_url': data['domain'],
    })

    send_mail(
        subject=data['subject'],
        message=strip_tags(email_content),
        html_message=email_content,
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[user.email],
    )