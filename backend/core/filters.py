import logging

from rest_framework import serializers, status
from rest_framework.response import Response


def process_filter(self, queryset, param, values):
    serializer = self.get_serializer(data={'param': param, 'values': values})
    try:
        serializer.is_valid(raise_exception=True)
    except serializers.ValidationError as e:
        return Response({"error": e.detail}, status=status.HTTP_400_BAD_REQUEST)

    cleaned_data = serializer.validated_data
    lookup = cleaned_data['lookup']
    values = cleaned_data['values']
    lookup_expression = cleaned_data['lookup_expression']

    if lookup in ['in', 'notIn']:
        values = values[0].split(',')
        if lookup == 'notIn':
            queryset = queryset.exclude(**{lookup_expression: values})
        else:
            queryset = queryset.filter(**{lookup_expression: values})
        return queryset

    if lookup == 'specified':
        values = values[0]
        if values.lower() in ['true', 'false']:
            values = [True] if values.lower() == 'true' else [False]

    filters = [{lookup_expression: value} for value in values]

    for filter in filters:
        value = filter[lookup_expression]
        if lookup in ['doesNotContain', 'notEquals']:
            queryset = queryset.exclude(**{lookup_expression: value})
        else:
            queryset = queryset.filter(**{lookup_expression: value})

    return queryset


class LoggingNoFileWatchingFilter(logging.Filter):
    def filter(self, record):
        return not (record.msg.startswith("File") or record.msg.startswith("Watching"))