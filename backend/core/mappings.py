field_mapping_products = {
    'code': 'code',
    'name': 'name',
    'description': 'description',
    'price': 'price',
    'owner': 'owner_id',
}
field_mapping_accounts = {
    'login': 'username',
    'firstName': 'first_name',
    'lastName': 'last_name'
}

lookups_mapping = {
    'contains': 'contains',
    'doesNotContain': 'contains',
    'equals': 'exact',
    'notEquals': 'exact',
    'specified': 'isnull',
    'in': 'in',
    'notIn': 'in'
}
