from datetime import datetime
import secrets
import string


def generate_key():
    alphabet = string.ascii_letters + string.digits
    key = ''.join(secrets.choice(alphabet) for _ in range(20))
    return key


def last_modified_by_fields(user):
    user.last_modified_by = user.username
    user.last_modified_date = datetime.now()
    user.save()
