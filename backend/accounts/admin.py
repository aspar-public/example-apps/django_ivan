from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from backend.accounts.models import User


class CustomUserAdmin(UserAdmin):
    list_display = ("id", "email", "is_staff", "is_active")
    list_filter = ("email", "is_staff", "is_active",)
    fieldsets = (
        (None, {"fields": ("username", "email", "password", 'first_name', 'last_name')}),
        ("Permissions", {"fields": ("is_staff", "is_active", "groups", "user_permissions")}),
    )
    add_fieldsets = (
        (None, {
            "classes": ("wide",),
            "fields": (
                "username", "email", "password1", "password2", "is_staff",
                "is_active", "groups", "user_permissions"
            )}
         ),
    )
    search_fields = ("email",)
    ordering = ("email",)

    def save_model(self, request, obj, form, change):
        # Set the 'last_modified_by' field to the user who made the change
        obj.last_modified_by = request.user.username
        super().save_model(request, obj, form, change)


admin.site.register(User, CustomUserAdmin)
