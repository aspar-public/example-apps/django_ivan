import os
from apscheduler.jobstores.redis import RedisJobStore
from apscheduler.schedulers.background import BackgroundScheduler
from .tasks import delete_unconfirmed_users


def start():
    scheduler = BackgroundScheduler()
    # scheduler.add_jobstore(RedisJobStore(), 'default')
    scheduler.add_job(delete_unconfirmed_users, 'interval', minutes=10)
    if os.environ.get('RUN_MAIN'):
        scheduler.start()



