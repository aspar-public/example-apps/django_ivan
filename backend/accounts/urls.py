from django.urls import path, include

from backend.accounts.views import UserRegisterApiView, UserDetailsUpdateDeleteApiView, VerifyEmail, \
    PasswordResetRequestApiView, PasswordResetConfirmApiView, \
    ChangePasswordApiView, MyTokenAuthView, AdminUserListView

urlpatterns = [
    path('admin/users', AdminUserListView.as_view(), name='admin_user_list'),
    path('register/', UserRegisterApiView.as_view(), name='user_register_api'),
    path('activate?key=<str:key>/', VerifyEmail.as_view(), name='user_verify_email'),
    path('authenticate/', MyTokenAuthView.as_view(), name='user_login_api'),
    path('account/', include([
        path("", UserDetailsUpdateDeleteApiView.as_view(), name='user_details_update_delete_api'),
        path('change-password/', ChangePasswordApiView.as_view(), name='change-password'),
        path('reset-password/init/', PasswordResetRequestApiView.as_view(), name='password_reset_request'),
        path('reset-password/finish/reset?key=<str:key>/', PasswordResetConfirmApiView.as_view(),
             name='password_reset_confirm'),
    ]
    ))

]
