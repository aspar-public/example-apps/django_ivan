from django.apps import AppConfig


class AccountsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'backend.accounts'

    def ready(self):
        from . import updater
        updater.start()

        import backend.accounts.signals
        result = super().ready()
        return result

