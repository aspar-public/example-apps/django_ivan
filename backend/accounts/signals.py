from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver

UserModel = get_user_model()


@receiver(signal=post_save, sender=UserModel)
def send_email_on_successful_register(instance, created, **kwargs):
    if created:
        pass


