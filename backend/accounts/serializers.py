from datetime import timedelta
from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from django.utils import timezone
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from datetime import datetime
from ..core.helpers import generate_key, last_modified_by_fields
from .models import User
from ..core.mappings import field_mapping_accounts, lookups_mapping
from ..core.validators import validate_password_max_len, validate_filtering_data

UserModel = get_user_model()


class UserRegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True,
                                     required=True,
                                     validators=[
                                         validate_password,
                                         validate_password_max_len])
    login = serializers.CharField(source='username',
                                  max_length=50)
    firstName = serializers.CharField(source='first_name',
                                      max_length=50,
                                      required=True,
                                      allow_blank=True)
    lastName = serializers.CharField(source='last_name',
                                     max_length=50,
                                     required=True,
                                     allow_blank=True)
    email = serializers.EmailField(max_length=191)
    imageUrl = serializers.CharField(source='image_url',
                                     max_length=256,
                                     required=True,
                                     allow_blank=True)
    langKey = serializers.ChoiceField(
        source='lang_key',
        choices=User.LANG_CHOICES,
        required=True,
        allow_blank=True)

    class Meta:
        model = User
        fields = ('login', 'firstName', 'lastName', 'email', 'imageUrl', 'langKey', 'password')

    def create(self, validated_data):
        user_obj = User.objects.create_user(**validated_data)

        return user_obj

    @staticmethod
    def validate_email(value):
        if UserModel.objects.filter(email=value).exists():
            raise serializers.ValidationError("Email address is already in use.")
        return value

    @staticmethod
    def validate_login(value):
        if UserModel.objects.filter(username=value).exists():
            raise serializers.ValidationError("Login is already in use.")
        return value

    @staticmethod
    def get_extra_fields(user):
        return {
            'id': user.id,
            'activated': user.is_active,
            'createdBy': user.created_by,
            'createdDate': user.created_date.isoformat(),
            'lastModifiedBy': user.last_modified_by,
            'lastModifiedDate': user.last_modified_date.isoformat(),
            'authorities': [group.name for group in user.groups.all()],
        }


class UserSerializer(serializers.ModelSerializer):
    login = serializers.CharField(source='username')
    firstName = serializers.CharField(source='first_name', allow_blank=True)
    lastName = serializers.CharField(source='last_name', allow_blank=True)
    imageUrl = serializers.URLField(source='image_url', allow_blank=True)
    activated = serializers.BooleanField(source='is_active')
    langKey = serializers.CharField(source='lang_key', allow_blank=True)
    createdBy = serializers.CharField(source='created_by')
    createdDate = serializers.DateTimeField(source='created_date', format='%Y-%m-%dT%H:%M:%S.%fZ')
    lastModifiedBy = serializers.CharField(source='last_modified_by')
    lastModifiedDate = serializers.DateTimeField(source='last_modified_date', format='%Y-%m-%dT%H:%M:%S.%fZ')
    authorities = serializers.SerializerMethodField()

    class Meta:
        model = UserModel
        fields = ['id', 'login', 'firstName', 'lastName', 'email', 'imageUrl', 'activated', 'langKey', 'createdBy',
                  'createdDate', 'lastModifiedBy', 'lastModifiedDate', 'authorities']

    @staticmethod
    def get_authorities(obj):
        return [group.name for group in obj.groups.all()]


class PasswordResetRequestSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate(self, attrs):
        email = attrs.get('email')

        try:
            user = UserModel.objects.get(email=email)
        except UserModel.DoesNotExist:
            raise serializers.ValidationError('User with this email does not exist.')

        user.reset_date = timezone.now()
        user.reset_key = generate_key()
        user.save()

        attrs['user'] = user
        return attrs


class PasswordResetConfirmSerializer(serializers.Serializer):
    key = serializers.CharField(source='reset_key')
    newPassword = serializers.CharField(write_only=True,
                                        source='new_password',
                                        validators=[validate_password, validate_password_max_len])

    class Meta:
        fields = ['key', 'newPassword']

    def validate(self, attrs):
        reset_key = attrs.get('reset_key')
        new_password = attrs.get('new_password')

        try:
            user = UserModel.objects.get(reset_key=reset_key)
            reset_date = user.reset_date

            expiration_time = timezone.now() - reset_date

            user.reset_key = None
            user.reset_date = None
            user.save()

            if expiration_time.total_seconds() > 24 * 3600:  # 24 hours
                raise serializers.ValidationError('Reset key has expired.')

            user.set_password(new_password)
            last_modified_by_fields(user)
        except UserModel.DoesNotExist:
            raise serializers.ValidationError(
                'This key is not valid. Please enter the valid key or make new request before proceeding!'
            )

        return attrs


class PasswordChangeSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True, write_only=True,
                                         validators=[validate_password, validate_password_max_len])
    confirm_password = serializers.CharField(required=True)

    def validate(self, attrs):
        old_password = attrs.get('old_password')
        new_password = attrs.get('new_password')
        confirm_password = attrs.get('confirm_password')

        user = self.context['request'].user
        if not user.check_password(old_password):
            raise serializers.ValidationError('Incorrect old password.')

        if new_password != confirm_password:
            raise serializers.ValidationError('New password and confirm password do not match.')

        user.set_password(new_password)
        last_modified_by_fields(user)
        return attrs


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    rememberMe = serializers.BooleanField(write_only=True, required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['rememberMe'].initial = False

    class Meta:
        model = User
        fields = ['username', 'password', 'rememberMe']

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        return token

    def validate(self, attrs):
        validated_data = super(MyTokenObtainPairSerializer, self).validate(attrs)
        token = self.get_token(self.user)

        remember_me = attrs['rememberMe']

        access_token = token.access_token

        if remember_me:
            access_token.set_exp(lifetime=timedelta(days=1))

        access_token.payload['sub'] = self.user.username
        access_token.payload['auth'] = [group.name for group in self.user.groups.all()][::-1]

        exp_time = access_token['exp']

        validated_data = {
            'id_token': str(access_token),
            'user_id': self.user.id,
            'username': self.user.username,
            'rememberMe': remember_me,
            'expiresAt': datetime.utcfromtimestamp(exp_time),
        }

        return validated_data


class AdminUserListSerializer(serializers.Serializer):
    def to_representation(self, instance):
        user_data = UserSerializer(source='*', read_only=True).to_representation(instance)

        return user_data

    def validate(self, data):
        validated_data = validate_filtering_data(self.initial_data, lookups_mapping, field_mapping_accounts)

        return validated_data
