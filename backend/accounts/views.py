import logging

from datetime import datetime

from django.contrib.auth import get_user_model
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import redirect

from rest_framework.authentication import SessionAuthentication
from rest_framework import generics
from rest_framework.decorators import permission_classes, authentication_classes
from rest_framework.pagination import PageNumberPagination

from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from rest_framework_simplejwt.views import TokenObtainPairView

from ..core.filters import process_filter
from ..core.helpers import last_modified_by_fields
from ..core.permissions import IsOwnerOrAdminOrReadOnly
from .serializers import UserRegisterSerializer, UserSerializer, PasswordResetRequestSerializer, \
    PasswordResetConfirmSerializer, PasswordChangeSerializer, MyTokenObtainPairSerializer, AdminUserListSerializer
from ..core.utils import send_email
from drf_yasg.utils import swagger_auto_schema

UserModel = get_user_model()

logger = logging.getLogger('django')


class UserRegisterApiView(APIView):
    permission_classes = (permissions.AllowAny,)
    authentication_classes = (SessionAuthentication,)

    @swagger_auto_schema(
        request_body=UserRegisterSerializer,
    )
    def post(self, request):
        serializer = UserRegisterSerializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
            user = serializer.create(validated_data=serializer.validated_data)
            extra_fields = serializer.get_extra_fields(user)
            response_data = {
                **serializer.data,
                **extra_fields
            }

            current_site = get_current_site(request)
            data = {
                'domain': current_site,
                'user': user,
                'html_template': 'account_activation_email.html',
                'relative_link': 'user_verify_email',
                'subject': 'APP account activation'
            }

            send_email(data)
            logger.info(f"User registration successful: {user.username}")

            return Response(response_data, status=status.HTTP_201_CREATED)
        except Exception as e:
            logger.warning(f"User registration failed: {str(e)}")

            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class VerifyEmail(APIView):
    permission_classes = [AllowAny, ]

    def get(self, request, *args, **kwargs):
        try:
            activation_key_from_email = kwargs['key']
            user = UserModel.objects.get(activation_key=activation_key_from_email)
            if user.is_active:
                return redirect('user_login_api')
            user.is_active = True
            user.activation_key = None
            last_modified_by_fields(user)

            return Response({'detail': 'Email successfully verified.'}, status=status.HTTP_200_OK)
        except UserModel.DoesNotExist:

            return Response({'error': 'Not found a valid key.'}, status=status.HTTP_400_BAD_REQUEST)


class UserDetailsUpdateDeleteApiView(generics.RetrieveUpdateDestroyAPIView):
    queryset = UserModel.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrAdminOrReadOnly]

    def get_object(self):
        return self.request.user

    def perform_update(self, serializer):
        serializer.validated_data['last_modified_by'] = self.request.user.username
        serializer.validated_data['last_modified_date'] = datetime.now()
        serializer.save()



@authentication_classes([])
@permission_classes([])
class PasswordResetRequestApiView(APIView):

    @swagger_auto_schema(
        request_body=PasswordResetRequestSerializer,
    )
    def post(self, request):
        serializer = PasswordResetRequestSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.validated_data['user']
            current_site = get_current_site(request)

            data = {
                'domain': current_site,
                'user': user,
                'html_template': 'password_reset_email.html',
                'subject': 'Account resset password'
            }
            send_email(data)

            return Response({'message': 'Password reset initiated. Check your email.'}, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@authentication_classes([])
@permission_classes([])
class PasswordResetConfirmApiView(APIView):

    @swagger_auto_schema(
        request_body=PasswordResetConfirmSerializer,
    )
    def post(self, request, *args, **kwargs):
        serializer = PasswordResetConfirmSerializer(data=request.data)
        if serializer.is_valid():
            return Response({'message': 'Password reset successful.'}, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChangePasswordApiView(generics.UpdateAPIView):
    permission_classes = [IsAuthenticated, IsOwnerOrAdminOrReadOnly]
    serializer_class = PasswordChangeSerializer

    def update(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={'request': self.request})
        if serializer.is_valid():
            return Response({'detail': 'Password changed successfully.'}, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MyTokenAuthView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


class AdminUserListViewPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 1000

    def paginate_queryset(self, queryset, request, view=None):
        ordered_queryset = queryset.order_by('id')

        return super().paginate_queryset(ordered_queryset, request, view)


class AdminUserListView(generics.ListAPIView):
    serializer_class = AdminUserListSerializer
    permission_classes = [IsAuthenticated, IsAdminUser]
    pagination_class = AdminUserListViewPagination

    def get_queryset(self):
        return UserModel.objects.all()

    def filter_queryset(self, queryset):
        search_params = self.request.query_params

        for param, values in search_params.lists():
            try:
                queryset = process_filter(self, queryset, param, values)
            except ValueError as e:
                raise ValueError(str(e))
        return queryset

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        try:
            queryset = self.filter_queryset(queryset)
        except ValueError as e:
            return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
        return self.list(request, queryset=queryset, *args, **kwargs)
