from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth import models as auth_models
from django.db import models

from ..core.helpers import generate_key
from ..core.managers import AppUserManager


class User(AbstractBaseUser, auth_models.PermissionsMixin):
    LANG_CHOICES = (
        ('en', 'English'),
        ('bg', 'Bulgarian'),
        ('fr', 'French'),
        ('de', 'German'),
    )

    id = models.BigAutoField(
        primary_key=True,
    )
    username = models.CharField(
        max_length=50,
        unique=True
    )
    password = models.CharField(
        max_length=67,
    )
    first_name = models.CharField(
        max_length=50,
        null=True,
        blank=True
    )
    last_name = models.CharField(
        max_length=50,
        null=True,
        blank=True
    )
    email = models.EmailField(
        max_length=191,
        unique=True
    )
    image_url = models.URLField(
        max_length=256,
        null=True,
        blank=True
    )
    is_active = models.BooleanField(
        default=False
    )
    lang_key = models.CharField(
        max_length=2,
        choices=LANG_CHOICES,
        null=True,
        blank=True
    )

    activation_key = models.CharField(
        max_length=20,
        null=True,
        blank=True
    )
    reset_key = models.CharField(
        max_length=20,
        null=True,
        blank=True
    )
    reset_date = models.DateTimeField(
        null=True,
        blank=True
    )
    created_by = models.CharField(
        max_length=50,
        default='anonymousUser'
    )
    created_date = models.DateTimeField(
        auto_now_add=True
    )
    last_modified_by = models.CharField(
        max_length=50,
        default='anonymousUser'
    )
    last_modified_date = models.DateTimeField(
        auto_now=True,
        null=True,
        blank=True
    )
    is_staff = models.BooleanField(
        default=False
    )
    groups = models.ManyToManyField(
        auth_models.Group,
        verbose_name=('groups'),
        blank=True,
        related_name='appuser_set_groups',  # Unique name for groups
    )

    user_permissions = models.ManyToManyField(
        auth_models.Permission,
        verbose_name=('user permissions'),
        blank=True,
        related_name='appuser_set_permissions',  # Unique name for user_permissions
    )

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    objects = AppUserManager()

    def save(self, *args, **kwargs):
        if not self.id:
            self.activation_key = generate_key()
        return super().save(*args, **kwargs)
