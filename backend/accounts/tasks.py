from django.contrib.auth import get_user_model
from django.utils import timezone

UserModel = get_user_model()


def delete_unconfirmed_users():
    one_hour_ago = timezone.now() - timezone.timedelta(hours=1)

    inactive_users = UserModel.objects.filter(is_active=False, created_date__lt=one_hour_ago)
    print(f'Deleting inactive registrations at {timezone.now()}')
    for user in inactive_users:
        print(user.username)
        user.delete()
