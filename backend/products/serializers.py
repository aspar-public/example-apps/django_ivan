from django.contrib.auth import get_user_model
from rest_framework import serializers

from backend.core.mappings import lookups_mapping, field_mapping_products
from backend.core.validators import validate_filtering_data

from backend.products.models import Product

UserModel = get_user_model()


class ProductSerializer(serializers.ModelSerializer):
    code = serializers.CharField(required=True,
                                 max_length=255)
    name = serializers.CharField(required=True,
                                 max_length=255)
    description = serializers.CharField(required=True,
                                        max_length=255,
                                        allow_blank=True)
    price = serializers.FloatField(required=True, allow_null=True)
    image01 = serializers.CharField(source='image_01', required=True, allow_null=True)
    image01ContentType = serializers.CharField(source='image_01_content_type',
                                               max_length=255,
                                               required=True,
                                               allow_blank=True)
    owner = serializers.PrimaryKeyRelatedField(queryset=UserModel.objects.all(), required=True, allow_null=True)

    class Meta:
        model = Product
        fields = ['id', 'code', 'name', 'description', 'price', 'image01', 'image01ContentType', 'owner']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if instance.image_01:
            representation['image01'] = [instance.image_01]
        if instance.owner:
            representation['owner'] = {'id': instance.owner_id, 'login': instance.owner.username}

        return representation


class ProductAddSerializer(ProductSerializer):
    pass


class ProductUpdateSerializer(ProductSerializer):
    code = serializers.CharField(required=True, allow_blank=True)
    name = serializers.CharField(required=True, allow_blank=True)


class ProductCountSerializer(serializers.Serializer):
    count = serializers.IntegerField()


class ProductListApiSerializer(serializers.Serializer):
    product_details = ProductSerializer(source='*', read_only=True)
    image01 = serializers.CharField(source='image_01', read_only=True)
    image01ContentType = serializers.CharField(source='image_01_content_type', read_only=True)

    class Meta:
        model = Product
        fields = ['id', 'code', 'name', 'description', 'price', 'image01', 'image01ContentType', 'owner']

    def to_representation(self, instance):
        product_details = self.fields['product_details'].to_representation(instance)
        return product_details

    def validate(self, data):
        additional_lookups = {
            'greaterThan': 'gt',
            'greaterThanOrEqual': 'gte',
            'lessThan': 'lt',
            'lessThanOrEqual': 'lte'
        }
        lookups_mapping.update(additional_lookups)

        validated_data = validate_filtering_data(self.initial_data, lookups_mapping, field_mapping_products)
        return validated_data
