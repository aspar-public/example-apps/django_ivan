from django.urls import path

from backend.products.views import ProductDetailsUpdateDeleteApiView, ProductCountApiView, ProductListCreateApiView

urlpatterns = [
    path('', ProductListCreateApiView.as_view(), name='product_add_api'),
    path('<int:pk>/', ProductDetailsUpdateDeleteApiView.as_view(), name='product_details_update_delete_api'),
    path('count/', ProductCountApiView.as_view(), name='products_count_view')
]
