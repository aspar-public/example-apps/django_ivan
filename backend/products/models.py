from django.contrib.auth import get_user_model
from django.db import models

UserModel = get_user_model()


class Product(models.Model):
    code = models.CharField(
        max_length=255,
        unique=True,
    )
    name = models.CharField(
        max_length=255
    )
    description = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    price = models.FloatField(
        null=True,
        blank=True
    )
    image_01 = models.CharField(
        null=True,
        blank=True,
    )
    image_01_content_type = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )
    owner = models.ForeignKey(
        UserModel,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
