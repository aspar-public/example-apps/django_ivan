from rest_framework import generics, status
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from backend.core.filters import process_filter
from backend.core.permissions import IsOwnerOrAdminOrReadOnly
from backend.products.models import Product
from backend.products.serializers import ProductCountSerializer, ProductListApiSerializer, \
    ProductUpdateSerializer, ProductAddSerializer


class ProductsListViewPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 1000

    def paginate_queryset(self, queryset, request, view=None):
        ordered_queryset = queryset.order_by('id')

        return super().paginate_queryset(ordered_queryset, request, view)


class ProductListCreateApiView(generics.ListCreateAPIView):
    serializer_class = ProductListApiSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = ProductsListViewPagination

    def get_queryset(self):
        return Product.objects.all()

    def filter_queryset(self, queryset):
        search_params = self.request.query_params
        for param, value in search_params.lists():
            try:
                queryset = process_filter(self, queryset, param, value)
            except ValueError as e:
                raise ValueError(str(e))
        return queryset

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        try:
            queryset = self.filter_queryset(queryset)
        except ValueError as e:
            return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
        return self.list(request, queryset=queryset, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = ProductAddSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response(data=serializer.data)


class ProductDetailsUpdateDeleteApiView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductUpdateSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrAdminOrReadOnly]

    def get_object(self):
        return self.queryset.filter(id=self.kwargs['pk']).first()


class ProductCountApiView(ProductListCreateApiView):

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        try:
            queryset = self.filter_queryset(queryset)
        except ValueError as e:
            return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)

        count = queryset.count()
        return Response({'count': count})
